package com.kindev.androidmvvmsmaple.main;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.kindev.androidmvvmsmaple.R;
import com.kindev.androidmvvmsmaple.details.DetailsActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupActionBar();
        MainViewModel viewModel = obtainViewModel(this);
        viewModel.getOpenDetailsEvent().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                if (integer != null) {
                    startDetailsActivity(integer);
                }
            }
        });
    }

    public static MainViewModel obtainViewModel(FragmentActivity activity) {
        return ViewModelProviders.of(activity).get(MainViewModel.class);
    }

    private void setupActionBar() {
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setTitle("Androiders");
        }
    }

    private void startDetailsActivity(int id) {
        startActivity(DetailsActivity.getLaunchIntent(this, id));
    }
}
