package com.kindev.androidmvvmsmaple.main;

import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;

import com.kindev.androidmvvmsmaple.data.model.Androider;

import java.util.List;

public class AndroidersBindings {

    @SuppressWarnings("unchecked")
    @BindingAdapter("app:items")
    public static void setItems(RecyclerView recyclerView, List<Androider> items) {
        MainAdapter adapter = (MainAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.setAndroiders(items);
        }
    }
}
