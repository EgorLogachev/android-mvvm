package com.kindev.androidmvvmsmaple.main;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;

import com.kindev.androidmvvmsmaple.SingleLiveEvent;
import com.kindev.androidmvvmsmaple.data.Callback;
import com.kindev.androidmvvmsmaple.data.Repository;
import com.kindev.androidmvvmsmaple.data.RepositoryFactory;
import com.kindev.androidmvvmsmaple.data.model.Androider;

import java.util.List;

public class MainViewModel extends ViewModel {

    private Repository repository = RepositoryFactory.getRepository();
    private final ObservableBoolean progress = new ObservableBoolean();
    private final ObservableArrayList<Androider> androiders = new ObservableArrayList<>();
    private final SingleLiveEvent<Integer> openDetailsEvent = new SingleLiveEvent<>();

    public ObservableBoolean getProgress() {
        return progress;
    }

    public ObservableArrayList getAndroiders() {
        return androiders;
    }

    public void start() {
        loadAndroiders();
    }

    public SingleLiveEvent<Integer> getOpenDetailsEvent() {
        return openDetailsEvent;
    }

    // navigation commands
    public void showDetails(int id) {
        openDetailsEvent.setValue(id);
    }

    // data uploading commands
    private void loadAndroiders() {
        progress.set(true);
        repository.getAndroiders(new Callback<List<Androider>>() {
            @Override
            public void onSuccess(List<Androider> result) {
                androiders.clear();
                androiders.addAll(result);
                progress.set(false);
            }

            @Override
            public void onError(String error) {
                progress.set(false);
            }
        });
    }
}
