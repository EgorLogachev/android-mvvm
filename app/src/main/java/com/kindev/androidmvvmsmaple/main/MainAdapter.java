package com.kindev.androidmvvmsmaple.main;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.kindev.androidmvvmsmaple.data.model.Androider;
import com.kindev.androidmvvmsmaple.databinding.ItemAndroiderBinding;
import com.kindev.androidmvvmsmaple.details.DetailsViewModel;

import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.AndroiderViewHolder> {

    private List<Androider> androiders;
    private OnItemClickListener listener;

    public MainAdapter(@Nullable OnItemClickListener listener) {
        this.listener = listener;
    }

    public void setAndroiders(List<Androider> androiders) {
        this.androiders = androiders;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AndroiderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemAndroiderBinding binding = ItemAndroiderBinding.inflate(inflater, parent, false);
        return new AndroiderViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AndroiderViewHolder holder, int position) {
        Androider androider = androiders.get(position);
        holder.binding.setViewModel(new DetailsViewModel(position, androider.getName()));
        holder.binding.setListener(listener);
    }

    @Override
    public int getItemCount() {
        return androiders == null ? 0 : androiders.size();
    }

    class AndroiderViewHolder extends RecyclerView.ViewHolder {

        ItemAndroiderBinding binding;

        public AndroiderViewHolder(ItemAndroiderBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
