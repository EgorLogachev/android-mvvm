package com.kindev.androidmvvmsmaple.main;

public interface OnItemClickListener {
    void onItemClick(int id);
}
