package com.kindev.androidmvvmsmaple.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kindev.androidmvvmsmaple.databinding.FragmentMainBinding;

public class MainFragment extends Fragment {

    private FragmentMainBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentMainBinding.inflate(inflater, container, false);
        binding.setViewModel(MainActivity.obtainViewModel(getActivity()));
        setupList(binding);
        return binding.getRoot();
    }

    private void setupList(final FragmentMainBinding binding) {
        binding.recyclerView.setAdapter(new MainAdapter(new OnItemClickListener() {
            @Override
            public void onItemClick(int id) {
                getViewModel().showDetails(id);
            }
        }));
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onResume() {
        super.onResume();
        getViewModel().start();
    }

    private MainViewModel getViewModel() {
        return binding.getViewModel();
    }


}
