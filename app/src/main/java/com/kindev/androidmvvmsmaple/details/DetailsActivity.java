package com.kindev.androidmvvmsmaple.details;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.kindev.androidmvvmsmaple.R;

public class DetailsActivity extends AppCompatActivity {

    private static final String ANDROIDER_ID_EXTRA_KEY = "androider_id_extra_key";

    public static Intent getLaunchIntent(Context context, int id) {
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(ANDROIDER_ID_EXTRA_KEY, id);
        return intent;
    }

    public static DetailsViewModel obtainViewModel(FragmentActivity activity) {
        return ViewModelProviders.of(activity).get(DetailsViewModel.class);
    }

    private void setupActionBar() {
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowHomeEnabled(true);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        setupActionBar();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            DetailsFragment fragment = DetailsFragment.newInstance(extras.getInt(ANDROIDER_ID_EXTRA_KEY));
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.contentFrame, fragment);
            transaction.commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
