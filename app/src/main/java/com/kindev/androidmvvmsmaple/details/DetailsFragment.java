package com.kindev.androidmvvmsmaple.details;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kindev.androidmvvmsmaple.databinding.FragmentDetailsBinding;

public class DetailsFragment extends Fragment {

    private static final String ANDROIDER_ID_BUNDLE_KEY = "androider_id_bundle_key";

    private FragmentDetailsBinding binding;

    public static DetailsFragment newInstance(int id) {
        Bundle args = new Bundle();
        args.putInt(ANDROIDER_ID_BUNDLE_KEY, id);
        DetailsFragment fragment = new DetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentDetailsBinding.inflate(inflater, container, false);
        DetailsViewModel viewModel = DetailsActivity.obtainViewModel(getActivity());
        binding.setViewModel(viewModel);
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle arguments = getArguments();
        if (arguments != null) {
            binding.getViewModel().start(arguments.getInt(ANDROIDER_ID_BUNDLE_KEY));
        }
    }
}
