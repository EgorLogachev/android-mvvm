package com.kindev.androidmvvmsmaple.details;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.kindev.androidmvvmsmaple.data.Callback;
import com.kindev.androidmvvmsmaple.data.Repository;
import com.kindev.androidmvvmsmaple.data.RepositoryFactory;
import com.kindev.androidmvvmsmaple.data.model.Androider;

public class DetailsViewModel extends ViewModel {

    private int id;
    private ObservableField<String> name = new ObservableField<>();
    private ObservableField<String> phone = new ObservableField<>();
    private ObservableField<String> birthday = new ObservableField<>();
    private ObservableBoolean progress = new ObservableBoolean();
    private Repository repository = RepositoryFactory.getRepository();

    public DetailsViewModel() { }

    public DetailsViewModel(int id, String name) {
        this.id = id;
        this.name.set(name);
    }

    public int getId() {
        return id;
    }

    public ObservableField<String> getName() {
        return name;
    }

    public ObservableField<String> getPhone() {
        return phone;
    }

    public ObservableField<String> getBirthday() {
        return birthday;
    }

    public ObservableBoolean getProgress() {
        return progress;
    }

    public void start(int id) {
        loadAndroider(id);
    }

    private void loadAndroider(int id) {
        progress.set(true);
        repository.getAndroider(id, new Callback<Androider>() {
            @Override
            public void onSuccess(Androider result) {
                progress.set(false);
                name.set(result.getName());
                phone.set(result.getPhone());
                birthday.set(result.getBirthday());
            }

            @Override
            public void onError(String error) {
                progress.set(false);
            }
        });
    }
}
