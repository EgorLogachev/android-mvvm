package com.kindev.androidmvvmsmaple.data;

public class RepositoryFactory {

    public static Repository getRepository() {
        return RepositoryImpl.getInstance();
    }
}
