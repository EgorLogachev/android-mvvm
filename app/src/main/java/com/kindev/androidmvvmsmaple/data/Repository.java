package com.kindev.androidmvvmsmaple.data;

import com.kindev.androidmvvmsmaple.data.model.Androider;

import java.util.List;

public interface Repository {

    void getAndroiders(Callback<List<Androider>> callback);

    void getAndroider(int id, Callback<Androider> callback);

}
