package com.kindev.androidmvvmsmaple.data;

import com.kindev.androidmvvmsmaple.data.model.Androider;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RepositoryImpl implements Repository {

    private volatile static RepositoryImpl instance;
    private ExecutorService executor = Executors.newFixedThreadPool(1);
    private volatile List<Androider> androiders;

    public static synchronized RepositoryImpl getInstance() {
        if (instance == null) {
            instance = new RepositoryImpl();
        }
        return instance;
    }

    private RepositoryImpl() {
        androiders = new ArrayList<Androider>() {{
            add(new Androider("Alexander Klimenko", "+38(095)093-5626", "1998/04/04"));
            add(new Androider("Antonina Legkaya", "+38(066)822-5836", "1984/05/16"));
            add(new Androider("Dmitriy Galkin", "+380505952903", "1988/08/06"));
            add(new Androider("Igor Kolesnyk", "+380508620154", "1997/08/04"));
            add(new Androider("Konstantin Kolesnikov", "+380505733099", "1977/09/26"));
            add(new Androider("Konstantyn Krasylnykov", "+380504065738", "1988/04/30"));
            add(new Androider("Marina Kravchenko", "+38 066 969 90 60", "1988/16/05"));
            add(new Androider("Nikita Bobkov", "+380994362963", "1994/10/28"));
            add(new Androider("Oleksandr Stafiievskyi", "+380638843059", "1993/12/23"));
            add(new Androider("Sergey Dorozhan", "+38(099) 543-18-59", "1995/10/20"));
        }};
    }

    @Override
    public void getAndroiders(final Callback<List<Androider>> callback) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                callback.onSuccess(androiders);
            }
        });
    }

    @Override
    public void getAndroider(final int id, final Callback<Androider> callback) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                callback.onSuccess(androiders.get(id));
            }
        });
    }
}
