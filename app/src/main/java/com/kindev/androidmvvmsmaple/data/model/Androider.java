package com.kindev.androidmvvmsmaple.data.model;

public class Androider {

    private String name;
    private String phone;
    private String birthday;

    public Androider(String name, String phone, String birthday) {
        this.name = name;
        this.phone = phone;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getBirthday() {
        return birthday;
    }
}
